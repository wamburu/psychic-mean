'use strict';

/**
 * Module dependencies.
 */
var config = require('./config/config'),
	mongoose = require('./config/lib/mongoose'),
	express = require('./config/lib/express'),
    colors  = require('colors'),
    logger  = require('mm-node-logger')(module);


// Initialize mongoose
mongoose.connect(function (db) {
	// Initialize express
	var app = express.init(db);

	// Start the app by listening on <port>
	app.listen(config.port);
   
    
    // Logging initialization
	console.log('Login application started on port ' + config.port);
   
});
